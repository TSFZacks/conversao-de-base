# Algoritmo para Conversão de Base - Método Divisão Sucessivas

## Descrição


## Como utilizar o CONVERSOR DE BASE

Para usar a biblioteca adicione o `convert.h` no seu código:

```c
#include <stdio.h>
#include "convert.h"
```

Para rodar exemplo, use o seguinte comando:

```bash
gcc ./src/main.c -I ./Include/ -o main
./main
```


## Autores
Esse algoritmo foi escrito por:


| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/11253331/avatar.png?width=400)  | Isaack Vinícius Silva do Amaral   | TSFZacks  | [isaackv.s.amaral@gmail.com](mailto:isaackv.s.amaral@gmail.com)
| ![](https://secure.gravatar.com/avatar/17038b3323f547679282a4cc1440b8fe?s=200&d=identicon)  | Bruno Nunes da Silva | brunonunesdasilva1   | [brunonunesdasilva83@gmail.com](mailto:brunonunesdasilva83@gmail.com)
| ![](https://secure.gravatar.com/avatar/de1bdeb13bbe97d6dc46db7b54df6f22?s=200&d=identicon)  | Jean Tiecher de Carvalho  | jeantiecher | [jeantiecher.carvalho@gmail.com](mailto:jeantiecher.carvalho@gmail.com)
| ![](https://secure.gravatar.com/avatar/f8f304af24e85d423ca8023e3e165f79?s=200&d=identicon)  | Bruno Tiecher  | BrunoTiecher | ----
| ![](https://gitlab.com/uploads/-/system/user/avatar/11526545/avatar.png?width=400)  | Eduardo Fernando Farfus  | eduardofarfus1 |[eduardofarfus@gmail.com](mailto:eduardofarfus@gmail.com)

