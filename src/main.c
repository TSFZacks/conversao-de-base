/* Codigo com a biblioteca convert.h
 *
 *
 *
 *
 * Autor:
 * Data
 */
#include "convert.h"
#define SIZE 8

void imprimaBin(int bin[], int length)
{
  for (int i = length-1; i >= 0; i--)
  printf("%d", bin[i]);
}

int main(int argc, char* argv[])
{
  int in[SIZE];
  int out;

  printf("Digite o valor em binário para conversão em decimal\n");

  for (int i = SIZE-1; i >= 0; i--)
  {
    printf("Digite o valor da posição %d\n", i);
    scanf("%d", &in[i]);
    if ((in[i]) < 0 || (in[i]) > 1)
    {
      printf("Valor incorreto, binário [0,1].\n");
      i++;
    }
  }

  out = bin2dec(in, SIZE);

  printf("A conversão de ");
  imprimaBin(in, SIZE);
  printf("::bin em decimal é: %d::dec.\n", out);

  return 0;
}
